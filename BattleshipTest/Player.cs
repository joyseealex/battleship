﻿using System;

namespace BattleShip
{
    /// <summary>
    /// Class for a single player battleship game
    /// </summary>
    public class StartGame
    {
        #region Method

        /// <summary>
        /// Entry point of the program
        /// </summary>
        /// <param name="args">Gets the arguments for the main function</param>
        public static void Main(string[] args)
        {
            var player = new Player();
            player.SetShipPosition();

            // There's only one ship which has 3 coordinates. If more coordinates are covered by the ship, then add it.
            while (player.GetAllHits() < 3)
            {
                player.GetCoordinates();
            }

            Console.WriteLine("Well Done! You won!");
            Console.WriteLine("Number of times you missed the hits: " + player.GetAllMisses());
        }

        #endregion
    }

    /// <summary>
    /// Class for the single player
    /// </summary>
    public class Player
    {
        #region Properties

        char[,] playerBoard = new char[10, 10];
        int x = 0;
        int y = 0;
        private int HitNumber = 0;
        private int MissNumber = 0;

        #endregion

        #region Methods

        /// <summary>
        /// Gets the hit count
        /// </summary>
        /// <returns>Returns hit count</returns>
        public int GetAllHits()
        {
            return HitNumber;
        }

        /// <summary>
        /// Gets all miss count
        /// </summary>
        /// <returns>Returns miss count</returns>
        public int GetAllMisses()
        {
            return HitNumber;
        }

        /// <summary>
        /// Sets the ship position by setting the coordinates in board
        /// </summary>
        public void SetShipPosition()
        {
            playerBoard[1, 3] = 'S';
            playerBoard[2, 3] = 'S';
            playerBoard[3, 3] = 'S';
        }

        /// <summary>
        /// Method to ask for coordinates to the player to get a hit or miss
        /// </summary>
        public void GetCoordinates()
        {
            try
            {
                Console.WriteLine("Hits: " + HitNumber);
                Console.WriteLine("Miss: " + MissNumber + '\n');
                Console.WriteLine("Enter x: ");
                string xCoord = Console.ReadLine();

                if (int.TryParse(xCoord, out int vaildNumber))
                {
                    if ( x < 0 || x > 9)
                        throw new Exception();

                    x = vaildNumber;
                }
                else
                {
                    throw new Exception();
                }

                Console.WriteLine("Enter y: ");

                string yCoord = Console.ReadLine();

                if (int.TryParse(yCoord, out vaildNumber))
                {
                    if (y < 0 || y > 9)
                        throw new Exception();

                    y = vaildNumber;
                }
                else 
                {
                    throw new Exception();
                }
                var aa = playerBoard[x, y].ToString();
                if (playerBoard[x, y].ToString().Equals("S"))
                {
                    playerBoard[x, y] = 'H';
                    Console.Clear();
                    Console.WriteLine("It was a Hit!\r\n");
                    HitNumber += 1;
                }
                else if (playerBoard[x, y].ToString().Equals("H") || playerBoard[x, y].ToString().Equals("M"))
                {
                    Console.Clear();
                    Console.WriteLine("Already marked!\r\n");
                }
                else
                {
                    playerBoard[x, y] = 'M';
                    Console.Clear();
                    Console.WriteLine("You Missed it!\r\n");
                    MissNumber += 1;
                }
            }
            catch
            {
                Console.Clear();
                Console.WriteLine("Coordinate(s) should be within a range of 0 - 9\n");
            }
        }

        #endregion
    }
}

